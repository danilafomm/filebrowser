package ru.fomin.fileBrowser.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;

public class DownloadFile {//принимать папку из конфига!

    public static void downloadFile(String downloadPath, String target) {
        String filename = downloadPath + "/" + new File(target).getName();


        File newFile = new File(isExistName(filename));
        try {
            FileUtils.copyFile(new File(target), newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String isExistName(String filename) {
        int counter=0;
        while (new File(filename).exists()){
            if(!FilenameUtils.getExtension(filename).equals("")) {
                if (counter > 0) {
                    int lenght = String.valueOf(counter).length();
                    filename = filename.substring(0, filename.lastIndexOf(".") - lenght) + counter + filename.substring(filename.lastIndexOf("."));
                } else {
                    filename = filename.substring(0, filename.lastIndexOf(".")) + counter + filename.substring(filename.lastIndexOf("."));
                }
            }else {
                if (counter > 0) {
                    int lenght = String.valueOf(counter).length();
                    filename = filename.substring(0, filename.length() - lenght) + counter;
                } else {
                    filename = filename + counter ;
                }
            }
            counter++;
        }
        return filename;
    }
}
