package ru.fomin.fileBrowser.util;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class TypeNameUtil {
    public static String getFileType(String target) {
        File directory = new File(target);
        if (directory.isDirectory()) {
            return "directory";
        } else if (target.endsWith("txt")) {
            return "txt";
        } else {
            return FilenameUtils.getExtension(directory.getAbsolutePath());
        }
    }

    public static String getFileName(String target){
        File file=new File(target);
        if (file.isDirectory()) {
            return file.getName();
        }
        return  FilenameUtils.getBaseName(file.getAbsolutePath());
    }
}
