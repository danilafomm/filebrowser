package ru.fomin.fileBrowser.util;

import ru.fomin.fileBrowser.Config;

import java.util.Map;

public class IconsMapping {
    private static Map<String,String> imgMapping = Config.get().getMapping();
    public static String getImg(String type){
        if (imgMapping.get(type) == null){
            return imgMapping.get("none");
        }
        return imgMapping.get(type);
    }
}
