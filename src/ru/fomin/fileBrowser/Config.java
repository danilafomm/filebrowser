package ru.fomin.fileBrowser;
import ru.fomin.fileBrowser.dao.DaoFileManager;
import ru.fomin.fileBrowser.dto.model.ListFiles;
import ru.fomin.fileBrowser.service.FileBrowseSerivce;
import ru.fomin.fileBrowser.service.IFileBrowseService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Config {

    private static final File PROPS = new File("D:\\java\\fileBrowser\\config\\browser.properties");
    private static final Config INSTANCE = new Config();

    private final String downloadDir;
    private final String initDir;
    private final IFileBrowseService fileBrowseSerivce;
    private final Map<String, String> mapping;


    private Config() {
        try (InputStream is = new FileInputStream(PROPS)) {
            Properties props = new Properties();
            props.load(is);
            Enumeration propertyNames = props.propertyNames();
            mapping = new HashMap<>();
            while (propertyNames.hasMoreElements()) {

                String key = (String) propertyNames.nextElement();
                String value = props.getProperty(key);

                if(key.equals("download.dir")||key.equals("init.dir")){
                    mapping.put(key, value);
                }else {
                    String[] arr = key.split("\\.");
                    for (String str : arr) {
                        mapping.put(str, value);
                    }
                }
            }
            downloadDir = mapping.get("download.dir");
            fileBrowseSerivce = new FileBrowseSerivce(
                    new DaoFileManager(
                            new ListFiles(
                                    new File(mapping.get("init.dir")))));
            initDir=mapping.get("init.dir");
            mapping.remove("download.dir");
            mapping.remove("init.dir");
        } catch (IOException e) {
            throw new IllegalStateException("Invalid config file " + PROPS.getAbsolutePath());
        }
    }

    public String getInitDir() {
        return initDir;
    }

    public static Config get() {
        return INSTANCE;
    }

    public String getDownloadDir() {
        return downloadDir;
    }

    public IFileBrowseService getFileBrowseSerivce() {
        return fileBrowseSerivce;
    }

    public Map<String, String> getMapping() {
        return mapping;
    }

    private static File getHomeDir() {
        String prop = System.getProperty("homeDir");
        File homeDir = new File(prop == null ? "." : prop);
        if (!homeDir.isDirectory()) {
            throw new IllegalStateException(homeDir + " is not directory");
        }
        return homeDir;
    }
}
