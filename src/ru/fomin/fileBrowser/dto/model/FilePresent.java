package ru.fomin.fileBrowser.dto.model;

import java.net.URLEncoder;

import org.apache.commons.io.FileUtils;
import ru.fomin.fileBrowser.dao.DaoNoteManager;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.logging.Logger;

public class FilePresent implements Comparable<FilePresent> {

    private static final Logger LOG = Logger.getLogger(FilePresent.class.getName());

    File ID ;//тут наверное лучше иметь абсолютный путь, чтобы привязаться к заметке
    String fileName;
    String fileType;
    boolean isWithNote;
    String size;
    String uri;
    int countNotes;

    public int getCountNotes() {
        return countNotes;
    }

    public FilePresent(File ID, String fileName, String fileType) {
        this.ID = ID;
        this.fileName = fileName;
        this.fileType = fileType;
        this.size=gettingSize();
        countNotes = DaoNoteManager.getNotes(ID.getAbsolutePath()).size();
        setUri();
    }

    public void setUri(){
        try {

            this.uri= URLEncoder.encode(ID.toString(), StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    public String getFileName() {
        return fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public File getID() {
        return ID;
    }

    public String getSize() {
        return size;
    }

    public String gettingSize(){
        Long size = FileUtils.sizeOf(ID);
        if(size>1000){
            size=size/1024;//kb
            if((size/1024)>1000){
                size=size/1024;//mb
                return size/1024+" mb";
            }return size+" kb";
        }else {
            return size+" byte";
        }
    }




    @Override
    public int compareTo(FilePresent o) {
        int cmp = fileName.compareTo(o.getFileName());
        return cmp !=0?cmp:fileType.compareTo(o.getFileType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilePresent that = (FilePresent) o;
        return isWithNote == that.isWithNote &&
                countNotes == that.countNotes &&
                Objects.equals(ID, that.ID) &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(fileType, that.fileType) &&
                Objects.equals(size, that.size) &&
                Objects.equals(uri, that.uri);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, fileName, fileType, isWithNote, size, uri, countNotes);
    }

    @Override
    public String toString() {
        return "FilePresent{" +
                "ID=" + ID +
                ", fileName='" + fileName + '\'' +
                ", fileType='" + fileType + '\'' +
                ", isWithNote=" + isWithNote +
                ", size='" + size + '\'' +
                ", uri='" + uri + '\'' +
                ", countNotes=" + countNotes +
                '}';
    }

    public String getUri() {
        return uri;
    }

}
