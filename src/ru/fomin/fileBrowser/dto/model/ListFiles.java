package ru.fomin.fileBrowser.dto.model;

import ru.fomin.fileBrowser.util.TypeNameUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ListFiles {
    private File mainDirectory;
    private List<FilePresent> directoryEntries = new ArrayList<>();
    private String uri;


    public ListFiles(File mainDirectory) {
        this.mainDirectory = mainDirectory;
        File[] files = mainDirectory.listFiles();
        for (File file : files) {
            directoryEntries.add(new FilePresent(file, TypeNameUtil.getFileName(file.getAbsolutePath()),
                    TypeNameUtil.getFileType(file.getAbsolutePath())));
        }
        setUri();
    }

    public File getMainDirectory() {
        return mainDirectory;
    }

    public void setUri(){
        try {

            this.uri= URLEncoder.encode(mainDirectory.toString(), StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public List<FilePresent> getDirectoryEntries() {
        return directoryEntries;
    }

    public String getUri() {
        return uri;
    }
}
