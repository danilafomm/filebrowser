package ru.fomin.fileBrowser.service;

import ru.fomin.fileBrowser.dao.DaoNoteManager;
import ru.fomin.fileBrowser.dao.IdaoFileManager;
import ru.fomin.fileBrowser.dto.model.FilePresent;
import ru.fomin.fileBrowser.dto.model.Note;

import java.io.File;
import java.util.List;

public class FileBrowseSerivce implements IFileBrowseService {
//    private String path;

    private IdaoFileManager idaoFileManager;

    public String geturi(){
        return idaoFileManager.getUri();
    }

    public String getCurremtFolder() {
        return idaoFileManager.getCurrentdirectory();
    }

            //=new DaoFileManager(new ListFiles(new File(path)));

//    public FileBrowseSerivce(String path) {
//        this.path = path;
//    }

    public FileBrowseSerivce(IdaoFileManager idaoFileManager) {
        this.idaoFileManager = idaoFileManager;
    }

    public List<FilePresent> delete(String target){
        idaoFileManager.deleteFile(target);
        DaoNoteManager.deleteNotes(new File(target).getAbsolutePath());
        return idaoFileManager.update();
    }

    @Override
    public List<FilePresent> update() {
        return idaoFileManager.update();
    }

    @Override
    public List<FilePresent> create(String filename, String filetype) {
        idaoFileManager.createFile(filename, filetype);
        return idaoFileManager.update();
    }

    @Override
    public <T> T open(String target) {
        return idaoFileManager.open(target);
    }

    @Override
    public List<Note> getNotes(String ID) {
        return DaoNoteManager.getNotes(ID);
    }

    @Override
    public void addNote(String ID,String text) {
        DaoNoteManager.addNote(ID,text);
    }

    @Override
    public List<FilePresent> up(String target) {
        idaoFileManager.up(target);
        return idaoFileManager.update();
    }


}
