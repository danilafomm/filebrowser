package ru.fomin.fileBrowser.service;

import ru.fomin.fileBrowser.dto.model.FilePresent;
import ru.fomin.fileBrowser.dto.model.Note;

import java.util.List;

public interface IFileBrowseService {
    public String getCurremtFolder();

    public List<FilePresent> delete(String target);

    public List<FilePresent> update();

    public List<FilePresent> create(String filename, String filetype);

    public <T> T open(String target);

    List<Note> getNotes(String ID);

    void addNote(String ID,String text);

    public List<FilePresent> up(String target);
    String geturi();
}
