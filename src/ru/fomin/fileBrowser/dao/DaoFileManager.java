package ru.fomin.fileBrowser.dao;

import org.apache.commons.io.FileUtils;
import ru.fomin.fileBrowser.dto.model.FilePresent;
import ru.fomin.fileBrowser.dto.model.ListFiles;
import ru.fomin.fileBrowser.util.DownloadFile;
import ru.fomin.fileBrowser.util.TypeNameUtil;


import java.io.File;
import java.io.IOException;
import java.util.List;

public class DaoFileManager implements IdaoFileManager {

    private ListFiles listFiles;


    public String getCurrentdirectory() {
        return listFiles.getMainDirectory().toString();
    }

    public DaoFileManager(ListFiles listFiles) {
        this.listFiles = listFiles;
    }


    @Override
    public void createFile(String filename, String filetype) {
        File directory = new File(DownloadFile.isExistName(filename));
        if (filetype.equals("file")) {
            try {
                directory.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            directory.mkdir();
        }
    }

    @Override
    public void deleteFile(String filename) {
        File fileToDelete = new File(listFiles.getMainDirectory(), filename);
        DaoNoteManager.deleteNotes(fileToDelete.getAbsolutePath());
        if(fileToDelete.isDirectory()) {
            try {
                FileUtils.deleteDirectory(fileToDelete);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            fileToDelete.delete();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T open(String target) {
        String fileType = TypeNameUtil.getFileType(target);
        switch (fileType) {
            case "txt":
                return (T) DaoTextManager.getText(target);
            case "directory":
                listFiles=new ListFiles(new File(target));
                return (T) listFiles.getDirectoryEntries();
            default:
                return (T) "I cant open it";
        }
    }

    @Override
    public void up(String target) {
            File file = new File(target);
            listFiles= new  ListFiles(file.getParentFile());
    }

    @Override
    public List<FilePresent> update() {
        listFiles=new ListFiles(listFiles.getMainDirectory());
        return listFiles.getDirectoryEntries();
    }

    @Override
    public String getUri() {
        return listFiles.getUri();
    }

}


