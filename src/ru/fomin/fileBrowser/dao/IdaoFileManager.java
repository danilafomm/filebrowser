package ru.fomin.fileBrowser.dao;

import ru.fomin.fileBrowser.dto.model.FilePresent;


import java.util.List;

public interface IdaoFileManager {

    String getCurrentdirectory();
    void createFile(String filename, String filetype);

    void deleteFile(String filename);

    <T> T open(String target);

    void up(String target);

    List<FilePresent> update();

    String getUri();


}
