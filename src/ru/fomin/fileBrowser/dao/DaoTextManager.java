package ru.fomin.fileBrowser.dao;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;


public class DaoTextManager {
    public static String getText(String target) {
        String text="";
        try (FileInputStream inputStream = new FileInputStream(target)) {
                text = IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static void saveText(String target, String text) {
        try {
            FileUtils.writeStringToFile(new File(target),text,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
