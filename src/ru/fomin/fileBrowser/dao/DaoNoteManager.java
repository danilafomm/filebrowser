package ru.fomin.fileBrowser.dao;

import ru.fomin.fileBrowser.dto.model.Note;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaoNoteManager {
    private List<Note> noteList = new ArrayList<>();
    private static Map<String, List<Note>> noteDB=new HashMap<>();

    public static void addNote(String ID, String text){
        noteDB.get(ID).add(new Note(text));
    }

    public static List<Note> getNotes(String ID){//здесь мы получаем список заметок
        if(!noteDB.containsKey(ID)){
            noteDB.put(ID, new ArrayList<Note>());
            return noteDB.get(ID);
        }
        return noteDB.get(ID);
    }

    public static void deleteNotes(String ID){
        noteDB.remove(ID);
    }

}
