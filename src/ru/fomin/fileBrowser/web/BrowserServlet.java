package ru.fomin.fileBrowser.web;

import ru.fomin.fileBrowser.Config;
import ru.fomin.fileBrowser.dao.DaoFileManager;

import ru.fomin.fileBrowser.dao.DaoNoteManager;

import ru.fomin.fileBrowser.dto.model.ListFiles;
import ru.fomin.fileBrowser.service.FileBrowseSerivce;
import ru.fomin.fileBrowser.service.IFileBrowseService;
import ru.fomin.fileBrowser.util.DownloadFile;
import ru.fomin.fileBrowser.util.IconsMapping;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import java.util.List;
import java.util.logging.Logger;

public class BrowserServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(BrowserServlet.class.getName());

    private String startPath;
    private String download;

    private IFileBrowseService fileBrowseSerivce;
    private IconsMapping iconsMapping = new IconsMapping();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.fileBrowseSerivce = Config.get().getFileBrowseSerivce();
        download = Config.get().getDownloadDir();
        startPath = Config.get().getInitDir();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String filename = request.getParameter("fileName");
        String filetype = request.getParameter("fileType");
        String currentDir = fileBrowseSerivce.getCurremtFolder();
        fileBrowseSerivce.create(currentDir + "/" + filename, filetype);
        response.sendRedirect("browser");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.severe("do Get entrance");
        String ID = "";
        if (request.getParameter("ID") != null) {
            ID = request.getParameter("ID");
        }
        LOG.severe("ID === " + ID);
        String action = request.getParameter("action");

        request.setAttribute("IconsMapping", iconsMapping);
        if (action == null) {
            request.setAttribute("listFiles", fileBrowseSerivce.update());
            request.setAttribute("currentPath", fileBrowseSerivce.geturi());
            LOG.severe("Current place" + fileBrowseSerivce.geturi());
            request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);
            LOG.severe("first initialization OK");
            return;
        }
        String currentDir = fileBrowseSerivce.getCurremtFolder();
        LOG.severe("up directory" + fileBrowseSerivce.getCurremtFolder());
        switch (action) {
            case "delete":
                request.setAttribute("listFiles", fileBrowseSerivce.delete(new File(ID).getName()));
                request.setAttribute("currentPath", fileBrowseSerivce.geturi());
                request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);
                return;
            case "open":
                Object openingObj = fileBrowseSerivce.open(ID);
                if (openingObj instanceof List) {
                    request.setAttribute("listFiles", openingObj);
                    returnTolist(request, response);
                    return;
//                } else if (openingObj instanceof String && openingObj.equals("I cant open it")) {//deprecated
//                    request.setAttribute("listFiles", fileBrowseSerivce.update());
//                    returnTolist(request, response);
                } else {
                    request.setAttribute("textTXT", openingObj);
                    request.setAttribute("fileName", ID);
                    request.setAttribute("currentPath", fileBrowseSerivce.geturi());
                    request.getRequestDispatcher("/WEB-INF/jsp/text.jsp").forward(request, response);
                    return;
                }
            case "up":
                if (new File(fileBrowseSerivce.getCurremtFolder()).getAbsolutePath().equals(new File(startPath).getAbsolutePath())) {
                    response.sendRedirect("browser");
                    return;
                }
                request.setAttribute("listFiles", fileBrowseSerivce.up(currentDir));
                returnTolist(request, response);
                return;
            case "note":
                request.setAttribute("listNotes", DaoNoteManager.getNotes(ID));
                request.setAttribute("fileName", ID);
                request.setAttribute("currentPath", fileBrowseSerivce.geturi());
                request.getRequestDispatcher("/WEB-INF/jsp/note.jsp").forward(request, response);
                return;
            case "download":
                DownloadFile.downloadFile(download, ID);
                response.sendRedirect("browser");
                return;
            default:
                throw new IllegalArgumentException("action " + action + " is illegal");
        }
    }

    private void returnTolist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("currentPath", fileBrowseSerivce.geturi());
        request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);
    }
}
