package ru.fomin.fileBrowser.web;

import ru.fomin.fileBrowser.dao.DaoNoteManager;
import ru.fomin.fileBrowser.dto.model.Note;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class NoteServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(NoteServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String filename = request.getParameter("file");
        String textTXT = request.getParameter("text");
        if (textTXT.length()>0) {
            LOG.severe("IN note ID" + filename);
            LOG.severe("IN note text" + textTXT);
            DaoNoteManager.addNote(filename, textTXT);
        }
        response.sendRedirect("browser");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
