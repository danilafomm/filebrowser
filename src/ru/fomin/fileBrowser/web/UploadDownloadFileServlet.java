package ru.fomin.fileBrowser.web;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class UploadDownloadFileServlet  extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(UploadDownloadFileServlet.class.getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        DiskFileItemFactory factory = new DiskFileItemFactory();

        ServletContext servletContext = this.getServletConfig().getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);

        ServletFileUpload upload = new ServletFileUpload(factory);

        String path = "";

        List  items = null;
        try {
            items = upload.parseRequest(request);
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        Iterator iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            if (item.isFormField()) {
                path = URLDecoder.decode(item.getString(), StandardCharsets.UTF_8.toString());
                LOG.severe("do post path: " + path);
            } else {
                File uploadedFile = new File(path+"/"+item.getName());
                LOG.severe("upload file" + uploadedFile.getAbsolutePath());
                try {
                    item.write(uploadedFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        response.sendRedirect("browser");
    }
}
