package ru.fomin.fileBrowser.web;


import ru.fomin.fileBrowser.dao.DaoTextManager;
import ru.fomin.fileBrowser.service.IFileBrowseService;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.logging.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TextServlet extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(TextServlet.class.getName());


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.severe("do Post entrance");
        request.setCharacterEncoding("UTF-8");
        String filename = request.getParameter("file");//не правильно видит файл, видит только папку
        String textTXT = request.getParameter("text");
        LOG.severe("filename  "+filename);
        LOG.severe("textTXT  "+textTXT);
        DaoTextManager.saveText(filename,textTXT);
        response.sendRedirect("browser");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
