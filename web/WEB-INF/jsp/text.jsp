
<%--
  Created by IntelliJ IDEA.
  User: danil
  Date: 10.10.2019
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <title>TXT файл</title>
</head>
<body>
<jsp:include page="fragments/header.jsp"/>
<section>
    <jsp:useBean id="fileName" scope="request" type="java.lang.String"/>
    <jsp:useBean id="textTXT" scope="request" type="java.lang.String"/>
    <form method="post" action="opentext" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="file" value="${fileName}">
        <textarea name="text" cols="100" rows=50>${textTXT}</textarea>

        <button type="submit">Save</button>
    </form>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
