
<%@ page import="ru.fomin.fileBrowser.dto.model.ListFiles" %>
<%@ page import="ru.fomin.fileBrowser.util.IconsMapping" %><%--
  Created by IntelliJ IDEA.
  User: danil
  Date: 28.09.2019
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <title>Список файлов по пути:</title>
</head>
<body>
<jsp:include page="fragments/header.jsp"/>
<section>
<%--    <jsp:useBean id="IconsMapping" scope="request" type="ru.fomin.fileBrowser.util.IconsMapping"/>--%>
    <table border="1" cellpadding="8" cellspacing="5">
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Size</th>
            <th>Delete</th>
            <th>Note</th>
            <th>Download</th>
        </tr>

        <%--@elvariable id="listFiles" type="java.util.List"--%>
        <c:forEach items="${listFiles}" var="linefile">
            <jsp:useBean id="linefile" type="ru.fomin.fileBrowser.dto.model.FilePresent"/>
            <tr>
                <c:choose>
                    <c:when test="${linefile.fileType=='directory'||linefile.fileType=='txt'}">
                        <td><a href="browser?ID=${linefile.uri}&action=open">
                            <img src=<%=ru.fomin.fileBrowser.util.IconsMapping.getImg(linefile.getFileType())%> width="30" height="30">
                                ${linefile.fileName}</a></td>
                    </c:when>
                    <c:otherwise>
                        <td><img src=<%=ru.fomin.fileBrowser.util.IconsMapping.getImg(linefile.getFileType())%> width="30" height="30">
                                ${linefile.fileName}</td>
                    </c:otherwise>

                </c:choose>
                <td>${linefile.fileType}</td>
                <td>${linefile.size}</td>
                <td><a href="browser?ID=${linefile.uri}&action=delete"><img src=<%=ru.fomin.fileBrowser.util.IconsMapping.getImg("delete")%> width="30" height="30"></a></td>
                <td><a href="browser?ID=${linefile.uri}&action=note">
                    <img src=<%=ru.fomin.fileBrowser.util.IconsMapping.getImg("note")%> width="30" height="30">${linefile.countNotes}</a></td>
                <c:if test="${linefile.fileType!='directory'}">
                <td><a href="browser?ID=${linefile.uri}&action=download"><img src=<%=ru.fomin.fileBrowser.util.IconsMapping.getImg("download")%> width="30" height="30"></a></td>
                </c:if>
            </tr>
        </c:forEach>
    </table>
    <c:if test="${listFiles.size()==0}">
        <li>There is empty</li>
    </c:if>



    <form method="post" action="browser" enctype="application/x-www-form-urlencoded">
        <input type="text" name="fileName" size="30"/>
        <select name="fileType">
            <option value="file">File</option>
            <option value="directory">Directory</option>
        </select>
        <button type="submit">Create</button>
    </form>
    <a href="browser?action=up">UP</a>


<%--  это для загрузки!--%>

    <form action="uploadDownload" method="post" enctype="multipart/form-data">

        <p>What file do you want to upload?</p>
        <input type="hidden" name="filePath" value="${currentPath}">
        <input type="file" name="fileToUpload">
        <%--@elvariable id="currentPath" type="java.lang.String"--%>
        <br/><br/>
        <input type="submit" value="Submit">
    </form>

</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
