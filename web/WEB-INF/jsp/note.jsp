
<%--
  Created by IntelliJ IDEA.
  User: danil
  Date: 10.10.2019
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <title>TXT файл</title>
</head>
<body>
<jsp:include page="fragments/header.jsp"/>
<jsp:useBean id="fileName" scope="request" type="java.lang.String"/>
<section>
    <h2>Notes of file "${fileName}"</h2>
    <table border="1" cellpadding="8" cellspacing="5">
        <tr>
            <th>Creation Date</th>
            <th>Text</th>
        </tr>
        <%--@elvariable id="listNotes" type="java.util.List"--%>
        <c:forEach items="${listNotes}" var="lineNote">
            <jsp:useBean id="lineNote" type="ru.fomin.fileBrowser.dto.model.Note"/>
            <tr>
                <td>${lineNote.date}</td>
                <td>${lineNote.text}</td>
            </tr>
        </c:forEach>
    </table>
    <c:if test="${listNotes.size()==0}">
        <li>There is empty</li>
    </c:if>

    <form method="post" action="note" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="file" value="${fileName}">
        <textarea name="text" cols="100" rows=50></textarea>

        <button type="submit">Save</button>
    </form>
</section>
<jsp:include page="fragments/footer.jsp"/>
</body>
</html>
