
<%@ page import="ru.fomin.fileBrowser.dto.model.ListFiles" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="ru.fomin.fileBrowser.service.FileBrowseSerivce" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%--
  Created by IntelliJ IDEA.
  User: danil
  Date: 29.08.2019
  Time: 19:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="currentPath" scope="request" type="java.lang.String"/>

<header><a href="browser">Файловая система по пути: <%=URLDecoder.decode(currentPath, StandardCharsets.UTF_8.toString()) %></a></header>

<hr/>
